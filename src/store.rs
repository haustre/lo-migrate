//! Storing of objects in S3

use crate::error::Result;
use crate::lo::{Data, Lo};
use atomic::Atomic;
use rusoto_s3::{
    AbortMultipartUploadRequest, CompleteMultipartUploadRequest, CompletedMultipartUpload,
    CompletedPart, CreateMultipartUploadRequest, PutObjectRequest, S3Client, UploadPartRequest, S3,
};
use std::fs::File;
use std::io::{self, Read};
use std::sync::atomic::Ordering;
use std::sync::Arc;

impl Lo {
    /// Store Large Object on S3
    ///
    /// Store Large Object data on S3 using the sha2 hash as key. The data in memory or the
    /// temporary file held by [`Data`] is dropped.
    pub fn store(
        &mut self,
        client: &S3Client,
        bucket: &str,
        chunk_size: usize,
        byte_counter: &Arc<Atomic<u64>>,
    ) -> Result<()> {
        let lo_data = self.take_lo_data();
        match lo_data {
            Data::File(ref temp) => {
                let mut file = File::open(&temp.path())?;
                if self.size() <= chunk_size as i64 {
                    #[allow(clippy::cast_sign_loss, clippy::cast_possible_truncation)]
                    let mut data = Vec::with_capacity(self.size() as usize);
                    file.read_to_end(&mut data)?;
                    assert_eq!(
                        self.size(),
                        data.len() as i64,
                        "unexpected size ({}) of buffer file for {:?}",
                        data.len(),
                        self
                    );
                    self.upload(data, client, bucket, byte_counter)
                } else {
                    self.upload_multipart(&mut file, client, bucket, chunk_size, byte_counter)
                }
            }
            Data::Vector(data) => self.upload(data, client, bucket, byte_counter),
            Data::None => panic!("Large Object must be fetched first"),
        }
    }

    fn upload(
        &self,
        data: Vec<u8>,
        client: &S3Client,
        bucket: &str,
        byte_counter: &Arc<Atomic<u64>>,
    ) -> Result<()> {
        let len = data.len();
        let request = PutObjectRequest {
            key: self.sha2_hex().expect("Large Object must be fetched first"),
            bucket: bucket.to_string(),
            body: Some(data.into()),
            content_type: Some(self.mime_type().to_string()),
            ..Default::default()
        };
        client.put_object(request).sync()?;
        byte_counter.fetch_add(len as u64, Ordering::Relaxed);
        Ok(())
    }

    fn upload_multipart<R>(
        &self,
        data: &mut R,
        client: &S3Client,
        bucket: &str,
        chunk_size: usize,
        byte_counter: &Arc<Atomic<u64>>,
    ) -> Result<()>
    where
        R: Read,
    {
        let key = self.sha2_hex().expect("Large Object must be fetched first");
        let upload = client
            .create_multipart_upload(CreateMultipartUploadRequest {
                key: key.clone(),
                bucket: bucket.to_string(),
                content_type: Some(self.mime_type().to_string()),
                ..Default::default()
            })
            .sync()?;

        let upload_id = upload.upload_id.expect("Missing upload ID");
        let mut buffer = vec![0; chunk_size];
        let mut tot_len: i64 = 0;
        let mut parts = Vec::new();
        for part in 1.. {
            match data.read(&mut buffer) {
                Ok(0) => break,
                Ok(len) => {
                    tot_len += len as i64;
                    let part =
                        self.upload_part(client, bucket, &key, &upload_id, part, &buffer[..len])?;
                    parts.push(part);
                    byte_counter.fetch_add(len as u64, Ordering::Relaxed);
                }
                Err(ref e) if e.kind() == io::ErrorKind::Interrupted => {
                    debug!("Interrupt while reading from buffer file of {:?}", self);
                }
                Err(e) => return Err(e.into()),
            }
        }

        assert_eq!(
            self.size(),
            tot_len,
            "Unexpected size ({}) of buffer file for {:?}",
            tot_len,
            self
        );

        client
            .complete_multipart_upload(CompleteMultipartUploadRequest {
                bucket: bucket.to_owned(),
                key: key.clone(),
                upload_id: upload_id.clone(),
                multipart_upload: Some(CompletedMultipartUpload { parts: Some(parts) }),
                ..Default::default()
            })
            .sync()?;

        Ok(())
    }

    fn upload_part(
        &self,
        client: &S3Client,
        bucket: &str,
        key: &str,
        upload_id: &str,
        part: i64,
        data: &[u8],
    ) -> Result<CompletedPart> {
        let resp = client
            .upload_part(UploadPartRequest {
                bucket: bucket.to_string(),
                key: key.to_owned(),
                upload_id: upload_id.to_owned(),
                part_number: part,
                body: Some(data.to_vec().into()),
                ..Default::default()
            })
            .sync();

        match resp {
            Ok(r) => Ok(CompletedPart {
                e_tag: r.e_tag.clone(),
                part_number: Some(part),
            }),
            Err(e) => {
                self.abort_upload(client, bucket, &key, &upload_id);
                Err(e.into())
            }
        }
    }

    fn abort_upload(&self, client: &S3Client, bucket: &str, key: &str, upload_id: &str) {
        let status = client
            .abort_multipart_upload(AbortMultipartUploadRequest {
                bucket: bucket.to_owned(),
                key: key.to_owned(),
                upload_id: upload_id.to_owned(),
                ..Default::default()
            })
            .sync();

        if let Err(e) = status {
            error!("failed to abort multipart upload for {:?}: {}", self, e);
        }
    }
}
