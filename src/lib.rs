//! Postgres to S3 migration tool for the Tocco Business Framework
//!
//! # Frankly, this library is design to do this using multiple threads
//!
//! * Fetch large object from Postgres
//! * Store binaries in S3
//! * Calculate sha2 hashes
//! * commit sha2 hashes to Postgres

#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![deny(unused_must_use)]
#![deny(const_err)]
#![deny(legacy_directory_ownership)]
#![deny(non_camel_case_types)]
#![deny(non_snake_case)]
#![deny(non_upper_case_globals)]
#![deny(patterns_in_fns_without_body)]
#![deny(private_in_public)]
#![deny(unused_must_use)]
#![deny(while_true)]
// clippy lints
#![warn(clippy::all)]
#![allow(clippy::new_without_default_derive)]
#![warn(clippy::cast_possible_truncation)]
#![warn(clippy::cast_possible_wrap)]
#![warn(clippy::cast_precision_loss)]
#![warn(clippy::cast_sign_loss)]
#![warn(clippy::empty_enum)]
#![warn(clippy::enum_glob_use)]
#![warn(clippy::float_arithmetic)]
#![warn(clippy::items_after_statements)]
#![warn(clippy::if_not_else)]
#![deny(clippy::mem_forget)]
#![warn(clippy::mut_mut)]
#![allow(clippy::needless_pass_by_value)]
#![warn(clippy::nonminimal_bool)]
#![warn(clippy::option_map_unwrap_or)]
#![warn(clippy::option_map_unwrap_or_else)]
#![warn(clippy::option_unwrap_used)]
#![deny(clippy::print_stdout)]
#![warn(clippy::result_unwrap_used)]
#![deny(clippy::unicode_not_nfc)]
#![deny(clippy::unseparated_literal_suffix)]
#![warn(clippy::single_match_else)]
#![deny(clippy::used_underscore_binding)]
#![deny(clippy::wrong_pub_self_convention)]
#![deny(clippy::wrong_self_convention)]

extern crate atomic;
extern crate chrono;
#[macro_use]
extern crate derive_error;
extern crate digest;
extern crate fallible_iterator;
#[macro_use]
extern crate log;
extern crate mkstemp;
extern crate postgres;
extern crate postgres_large_object;
extern crate rusoto_core;
extern crate rusoto_credential;
extern crate rusoto_s3;
extern crate rustc_serialize as serialize;
extern crate sha1;
extern crate two_lock_queue;

mod commit;
pub mod error;
mod lo;
mod receive;
mod store;
pub mod thread;
pub mod utils;

pub use crate::lo::Lo;
